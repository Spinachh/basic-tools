# -*-coding:utf-8 -*-
# Project: DataTagProcess
# Author:mongoose
# Date:2022/7/18(code review)

import os
import time
import logging
import pandas as pd
import xlwt
from nltk import word_tokenize
from nltk.corpus import stopwords
from tag_keywords_v2 import tag_keyword


logging.basicConfig(level=logging.INFO, format='%(asctime)s-:%(message)s')


class DataProcessTag(object):

    @staticmethod
    def read_file():

        path = os.getcwd()
        file_names = os.listdir(path)
        for file_name in file_names:
            if 'xlsx' in file_name:
                file = file_name.split('.xlsx')[0]  # get file name
                # file = 'facebook'
                ori = pd.read_excel(io=file + '.xlsx')

                data = ori.iloc[0:, 0:2]  # [row, col] # get col(2) data
                # print(data[0:9])
                # print(data.index)

                return file, data

    @staticmethod
    def create_workbook():

        workbook = xlwt.Workbook(encoding='utf-8')
        book_sheet = workbook.add_sheet('sheet', cell_overwrite_ok=True)

        book_sheet.write(0, 0, 'account_name')  # create csv header
        book_sheet.write(0, 1, 'text')
        book_sheet.write(0, 2, 'tag')

        return workbook, book_sheet

    @staticmethod
    def tag_process(file, data, book_sheet, workbook):

        format_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        cnt_row = 0

        for index, row in data.iterrows():  # cron write index ,row_data
            cnt_row += 1
            row_name = row['account']
            row_text = str(row['text'])

            # tag
            tag_lst = []
            # tag_keys = tag_keyword.keys()

            if row_text:
                text_words = word_tokenize(row_text)    # cut words

                inter_punctuations = [',', '.', ':', ';', '?', '(', ')',
                                      '[', ']', '&', '!', '*', '@', '#', '$', '%']

                cut_words = [word for word in text_words if word not in
                             inter_punctuations]

                stops = set(stopwords.words("english"))     # english stop words
                # filter word
                cut_words_final = [word for word in cut_words if word not in stops]

                for word in cut_words_final:
                    # if word in tag_keys:
                    for key, value in tag_keyword.items():   # jude word in tag_keyword
                        if '"' + word + '"' in value:
                            tag_lst.append(key)
                tag_lst = '、'.join(list(set(tag_lst)))
            else:
                tag_lst = 'None'

            book_sheet.write(cnt_row, 0, row_name)      # write row name
            book_sheet.write(cnt_row, 1, row_text)      # write text data
            book_sheet.write(cnt_row, 2, str(tag_lst))  # write tag data
            logging.info('file:{} row:{} was finished!'.format(file, cnt_row))

        workbook.save(file + '-tag-v3-{}.xls'.format(format_time))  # save csv file

    @staticmethod
    def test_dict():
        for key, value in tag_keyword.items():
            print(key, value)
            if '"panda"' in value:
                print(key)

    def start_run(self):
        file, data = self.read_file()
        workbook, book_sheet = self.create_workbook()
        self.tag_process(file, data, book_sheet, workbook)


if __name__ == '__main__':
    data_process_tag = DataProcessTag()
    data_process_tag.start_run()