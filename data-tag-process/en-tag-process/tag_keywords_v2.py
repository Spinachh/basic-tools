# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/5/25


tag_keyword = {
    "其他": '["astronaut", "Shenzhou", "sport", '
          ' "Cup", "ball", "March Madness", "space", '
          ' "tech", "hack", "Games", "robot", "3D", '
          ' "navigate", "intelligent", "satellite", '
          ' "champion", "olympic", "lab", "player", '
          ' "athlete", "scientist", "record", "AI",]',

    "自然": '["flood", "drought", "earthquake", "landscape", '
          '"earth", "landslide", "rainfall", "flooding", "shark",'
          ' "burn", "tornado", "spring", "blossom", "flower",'
          ' "diversity", "garden", "plastic", "ecology", "nature",'
          ' "mountain", "lake", "storm", "water", "warming", "zoo",'
          ' "mount", "forest", "environment", "natural", "fish",'
          ' "climate", "reserve", "botanical", "bird", "tree",'
          ' "panda", "village", "sea level", "plant", "green"]',

    "社会": '["Covid-19", "refugee", "humanitarian", "test positive",'
          ' "human", "shooting", "caught", "guilty", "criminal", "shot",'
          ' "judge", "attorney", "death", "prosecutor", "dead", "wound",'
          ' "police", "kill", "gun", "protest", "injure", "rescue",'
          ' "kidnapp", "died", "survivor", "LGBT", "store", "gay",'
          ' "court", "lockdown", "vaccinate", "Wechat", "health",'
          ' "job", "airline", "tourist", "victim", "autism", "missing",'
          ' "pandemic", "humanity", "law", "legal", "vaccine", "TikTok",'
          ' "tour", "travel", "social", "society", "day", "festival",'
          ' "movement", "ceremony", "ceremonial", "disease", "visit", "Shanghai",]',

    "文化": '["poem", "debut", "writer", "writing", "music", "concert",'
          ' "Coachella", "film", "movie", "actor", "award", "actress",'
          ' "photographer", "book", "Pope", "archaeological", "stage",'
          ' "artist", "museum", "culture", "cultural", "religion", "oscar",'
          ' "Muslim", "history", "historical", "exhibition",]',

    "经济": '["Wall street", "price", "inflation", "oil", "energy",'
          ' "Boao", "WTO", "Elon Musk", "Amazon", "economic", "economy",'
          ' "budget", "solar", "invest", "trade", "market", "industry",'
          ' "cost", "profit", "vehicle", "import",]',

    "政治": '["election", "party", "republican", "Pakistan", "Ukraine",'
          ' "Russia", "Zelensky", "Putin", "Biden", "Trump",'
          ' "Boris Johnson", "Macron", "sanction", "NATO", "corruption",'
          ' "United Nations", "chief", "Antonio Guterres", "Ivanka",'
          ' "political", "politicial", "bill", "Kyiv", "White House",'
          ' "minister", "Chernobyl", "relation", "ASEAN", "policy",'
          ' "Gov", "EU", "war", "council", "parliament", "legislation",'
          ' "legislative", "parties", "politician", "spokesperson",'
          ' "ambassador", "embassy", "military", "premier", "nuclear", "U.S.",]'
}
