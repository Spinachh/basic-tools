# !/usr/bin/python3
# Author : mongoole
# Date : 2022/07/17

import io
import os
import sys
import time
import shutil
import pandas as pd
import logging

# num	name	news_title	content1	content2	content3

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

file_time = time.strftime("%Y-%m-%d", time.localtime())

fpath = r'E:\kexieproject\keyword-filter\q2.xlsx'  # data result directory

df = pd.read_excel(fpath)

col_name = df.columns.tolist()  # 取出全部列名存放在列表

col_name.append('flag')  # 将新增的列放置最后

df = df.reindex(columns=col_name)

# data = df.iloc[0:2, 2:6]  # 前两行和第3列至第5列
data = df.iloc[0:, 0:]

# print(data)

keywords = [
    "次会议", "中央有关规定精神", "第二十次代表大会", "绿水青山才是金山银山",
    "吉森省", "指示批示", "二十大会议", "宁夏自治区", "建党100年", "党史教育活动",
    "全国人大常委", "社会义", "党的二十大报告精神", "总书记讲话精神", "习近平总书记的讲话",
    "习总书记", "以习近平总书记为核心的党中央", "党史学习教育活动", "中共中国",
    "新疆自治区", "届全国政协", "中央", "“不忘初心、牢记使命”主题教育活动",
    "市十三次党代会", "卫建委", "十九大大精神", "主题教育活动", "习近平总书记生态文明思想",
    "人大人大常委会", "讲话精神", "《党章》", "开幕式", "市十二次党代会",
    "习近平新时代中国特色这会主义思想", "“一带一路”战略", "欧阳晓辉", "全国人大副委员长",
    "习近平总书记关于宣传思想工作重要思想", "习近平新时代中国特色主义思想",
    "《科技进步法》", "不忘初心、牢记始命", "中央经济会议", "庆祝建国70周年",
    "筑牢中华民族共同体意识", "党的十九大及十九届历次全会精神", "人大常委",
    "建国70周年", "人大常委会常委", "二十", "党的十九大、", "党的十九大及",
    "六中会会", "建设兵团", "党徽", "习总书记", "习书记", "主题教育活动",
]

new_col_content = []

cn_num = 0

for index, row in data.iterrows():
    # row_num = row['num']
    # row_name = row['name']
    title = row['news_title']
    content1 = row['content1']
    content2 = row['content2']
    content3 = row['content3']

    # print(row_num)
    # print(title)
    # print(content1)
    # print(content2)
    # print(content3)
    # breakpoint()
    key_lst = []
    for key in keywords:
        try:
            if key in title:
                flag = 'True'
                key_lst.append(flag + '##' + key)
            elif key in content1:
                flag = 'True'
                key_lst.append(flag + '##' + key)
            elif key in content2:
                flag = 'True'
                key_lst.append(flag + '##' + key)
            elif key in content3:
                flag = 'True'
                key_lst.append(flag + '##' + key)
            # else:
            #     flag = 'False'
            #     key_lst.append(flag + key)

        except:
            key_lst.append('False')
            pass
    # print(key_lst)
    # breakpoint()
    with open('keys_filter_flag.txt', 'a', encoding='utf-8') as f:
        if 'True' in key_lst:
            new_col_content.append('True')
            f.write('True' + '####' + str(key_lst) + '\n')
        else:
            new_col_content.append('False')
            f.write('False' + '\n')

    cn_num += 1
    logging.warning('Row : {} finished.'.format(cn_num))

# print(new_col_content)
# 重新构建索引值

# df['flag'] = new_col_content
# 重新写入新的excel
# df.to_excel('new1_excel.xlsx')