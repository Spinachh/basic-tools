# -*-coding:utf-8 -*-
# Project:
# Author:mongoole
# Date:2022/11/23

import os
import time
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
file_time = time.strftime("%Y-%m-%d", time.localtime())

img_dir_name = 'G:\\mongoole\\Data-Target\\'  # img directory name
# img_dir_name = 'I:\\gfc\\修改名称后的照片\\'  # img directory name
imglist = os.listdir(img_dir_name)

for img in imglist:
    # print(img)
    # print(type(img))
    # breakpoint()
    if '（' in img:
        dst_img = img.replace('（', '(').replace('）', ')')
        try:
            os.rename(img_dir_name + img, img_dir_name + dst_img)
        except:
            logging.warning('Img: {} Exsit!'.format(img))
        with open(r'sys-no\result-2022-11-25.txt', 'a', encoding='utf-8') as f:
            f.write(img + '\n')