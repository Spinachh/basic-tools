# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongooses
# Date : 2022/4/8


def convert_time_to_str(time):

    # 时间数字转化成字符串，不够10的前面补个0
    if time < 10:
        time = '0' + str(time)
    else:
        time = str(time)
    return time


def sec_to_data(y):

    h = int(y//3600 % 24)
    d = int(y // 86400)
    m = int((y % 3600) // 60)
    s = round(y % 60, 2)
    h = convert_time_to_str(h)
    m = convert_time_to_str(m)
    s = convert_time_to_str(s)
    d = convert_time_to_str(d)
    # 天 小时 分钟 秒
    # return d + "天" + ":" + h + "小时" + ":" + m + "分钟" + ":" + s + "秒"
    return d + "天" + h + "小时" + m + "分钟" + s + "秒"


if __name__ == '__main__':
    a = sec_to_data(8646)  # 86461秒
    print(a)
