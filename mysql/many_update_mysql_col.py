# -*- coding:utf-8 -*-
# Project : KeXie
# Author : mongoole
# Date : 2023-03-31

import pymysql

db = pymysql.connect("127.0.0.1", "root", "mongoole", "weixin_spider")


def conn_mysql(sql):
    try:
        cursor = db.cursor()

        # 科协数据导出样例
        cursor.execute(sql)

        # data = cursor.fetchone()  #查询一条测试使用
        all_search_data = cursor.fetchall()
        return all_search_data
    except:
        return "Error"

#http://mp.weixin.qq.com/s?__biz=MzI5MjEwMjg5NQ==&mid=2649822288&idx=1&sn=5c043abc8479e81feb424b25ce025a3d
# &chksm=f403c3d0c3744ac6b5d363dd33014567555c53ff23860fb4a29ed78c9298535cf9729b1a9567&scene=27#wechat_redirect |


for id in range(1, 644):
    try:
        sql = "select article_content_url from wx_article where id = %d;" % (id)
        search_data = conn_mysql(sql)
        if search_data[0][0] != '':
            source_data = search_data[0][0]
            replace_data = source_data.split('&chksm')[0]
            sql1 = "update wx_article set article_content_url = replace(article_content_url,'%s','%s') where id=%d;"
            conn = db.cursor()
            conn.execute(sql1 % (source_data, replace_data, id))
            db.commit()
            conn.close()
    except:
        print('修改失败', 'id:', id)