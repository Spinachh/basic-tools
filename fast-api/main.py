#!/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoose
# Date : 2022/7/8 


from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from database import session
from models import Account, Article

# Base是用来给模型类继承的，类似django中的models.Model

# 模型类，tablename指表名，如果数据库中没有这个表会自动创建，有表则会沿用


# class Account(Base):
#     __tablename__ = "wx_account"
#     __table_args__ = {
#         "useexisting": True,
#         'mysql_charset': 'utf8mb4'
#     }
#
#     id = Column(Integer, primary_key=True)
#     account_name = Column(String(50))  # 公众号名称
#     account_id = Column(String(30), unique=True)  # 公众号唯一id
#     account_biz = Column(String(20), unique=True)  # 公众号__biz
#     account_id_unique = Column(String(30), unique=True)  # 公众号唯一id
#     account_logo = Column(String(300), unique=True)  # 公众号头像
#     account_desc = Column(String(300))  # 公众号描述
#     account_url = Column(String(500), unique=True)  # 公众号解析链接
#     created = Column(String(20), default=str(int(time.time())))  # 公众号添加时间
#     status = Column(Integer, default=0)  # 状态0-未运行，1-等待中，2-运行中，3-已暂停
#     offset = Column(Integer, default=0)  # 公众号偏移量
#     counts = Column(Integer, default=0)  # 公众号获取的文章数量
#     end = Column(Boolean, default=False)  # 公众号爬取是否完毕
#     fail = Column(Boolean, default=False)  # 公众号有效性
#     update = Column(String(20), default="1356969600")  # 公众号更新时间
#     project_name = Column(String(50))  # 项目名称
#     project_status = Column(String(50))  # 项目账号是否启用
#
#     def __repr__(self):
#         return "<Account: %s %s %s %s>" % (self.account_name, self.account_id, self.offset, self.end)


# 此步也必不可少
app = FastAPI()


@app.get("/account")
async def account_id():
    account_lst = session.query(Account).all()
    return account_lst


@app.get("/account/list/{account_id}")
async def account(account_id: int):
    account_lst = session.query(Account).filter(Account.id == account_id).all()
    session.close()
    return account_lst


@app.get("/account/articles")
async def articles(skip: int = 0, limit: int = 10):
    articles_lst = session.query(
        Article).order_by(
        Article.id.desc()
    ).all()
    session.close()
    return articles_lst[skip: skip + limit]


@app.get("/")
async def wx():
    return {"Hello": "World"}

