# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/7/8 

import time
from sqlalchemy import create_engine
from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from database import engine

Base = declarative_base()
Base.metadata.create_all(bind=engine)


class Account(Base):
    __tablename__ = "wx_account"
    __table_args__ = {
        "useexisting": True,
        'mysql_charset': 'utf8mb4'
    }

    id = Column(Integer, primary_key=True)
    account_name = Column(String(50))  # 公众号名称
    account_id = Column(String(30), unique=True)  # 公众号唯一id
    account_biz = Column(String(20), unique=True)  # 公众号__biz
    account_id_unique = Column(String(30), unique=True)  # 公众号唯一id
    account_logo = Column(String(300), unique=True)  # 公众号头像
    account_desc = Column(String(300))  # 公众号描述
    account_url = Column(String(500), unique=True)  # 公众号解析链接
    created = Column(String(20), default=str(int(time.time())))  # 公众号添加时间
    status = Column(Integer, default=0)  # 状态0-未运行，1-等待中，2-运行中，3-已暂停
    offset = Column(Integer, default=0)  # 公众号偏移量
    counts = Column(Integer, default=0)  # 公众号获取的文章数量
    end = Column(Boolean, default=False)  # 公众号爬取是否完毕
    fail = Column(Boolean, default=False)  # 公众号有效性
    update = Column(String(20), default="1356969600")  # 公众号更新时间
    project_name = Column(String(50))  # 项目名称
    project_status = Column(String(50))  # 项目账号是否启用

    def __repr__(self):
        return "<Account: %s %s %s %s>" % (self.account_name, self.account_id, self.offset, self.end)


class Article(Base):
    __tablename__ = "wx_article"
    __table_args__ = {
        "useexisting": True,
        'mysql_charset': 'utf8mb4'
    }

    id = Column(Integer, primary_key=True)
    article_title = Column(String(200))
    article_author = Column(String(50))
    article_publish_time = Column(String(20))
    article_copy_right = Column(Boolean)
    article_digest = Column(String(300))
    # article_html = Column(mysql.MSMediumText)

    article_content_url = Column(String(500), unique=True)
    # article_cover_url = Column(String(500))
    # article_source_url = Column(String(500))
    # article_fail = Column(Boolean, default=False)  # 文章有效性
    article_done = Column(Boolean, default=False)  # 文章内容是否抓取
    # article_comment_id = Column(String(20))
    # comment_update = Column(String(20), default="1356969600")  # 公众号更新时间
    # read_like_update = Column(String(20), default="1356969600")  # 公众号更新时间
    read_count = Column(Integer, default=0)
    like_count = Column(Integer, default=0)
    digg_count = Column(Integer, default=0)
    new_comment_count = Column(Integer, default=0)
    # comment_count = Column(Integer, default=0)
    # comments = relationship("Comment", backref="wx_article")
    # account_id = Column(Integer, Base.ForeignKey("wx_account.id"))  # 公众号唯一id

    def __repr__(self):
        return "<Title: %r, Publish: %s>" % (
                   self.article_title,
                   time.strftime('%Y-%m-%d', time.localtime(self.article_publish_time)),

               )