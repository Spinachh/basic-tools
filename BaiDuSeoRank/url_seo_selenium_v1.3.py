# -*- CODING:UTF-8 -*-
# AUTHOR: mongoole
# DATE: 2023-12-21

# ALTER OUT_LINK360


import io
import sys
import re
import csv
import time
import datetime
import logging
import pymongo
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_seo_rank
    collection = db.seo_rank_2023_Q4
    return collection


def get_source_code(url, item):
    # url = 'https://seo.chinaz.com/{}'.format(url)
    driver = webdriver.Chrome()
    driver.get(url)
    driver.maximize_window()
    time.sleep(0.5)
    source_code = driver.page_source
    name = item.strip().replace('.', '-')
    file_time = time.strftime("%Y-%m-%d", time.localtime(time.time()))
    with open(r'website-txt/{}-{}.txt'.format(name, file_time), 'w', encoding='utf-8') as f:
        f.write(source_code)

    driver.close()


def get_weight(url, source_code, collect, website_name):
    seo_dict_name = [
        'WebSiteName',
        'AllFlowTotal', 'BaiDuPCRank', 'BaiDuMobileRank', 'SoGoPCRank',
        'BingPCRank', 'So360PCRank', 'SMPCRank', 'WordRank', 'WebClassfiy',
        'DomainRegister', 'RegisterMail', 'DomainAge', 'DomainRecord', 'DomainName',
        'DomainGroup', 'DomainExamineTime', 'DomainIp', 'SameIpDomain', 'DomainSpeed',
        'CompetitionDomain', 'PCKWCount', 'MoBileKWCount', 'FirstPageLocation',
        'OutLinkCount', 'SeoSiteIndex', 'SeoBaiDuSiteIndex', 'OutBaiDuLink', 'SeoSoGoSiteIndex',
        'OutSoGoLink', 'Seo360SiteIndex', 'Out360Link', 'SeoBingSiteIndex',
        'OutBingLink', 'SeoGoogleSiteIndex', 'OutGoogleLink',
    ]
    try:
        AllFlowTotal = re.findall(r'href="//rank.chinaz.com/all/(.*?)</', source_code, re.S | re.M)[0]
        AllFlowTotal = AllFlowTotal.split('>')[-1]
    except:
        AllFlowTotal = ''
    # print('AllFlowTotal: ', AllFlowTotal)

    Rank = re.findall(r'<img src="//csstools.chinaz.com/tools/images/rankicons(.*?) class="mt"',
                      source_code, re.S | re.M)
    try:
        BaiDuPCRank = re.findall(r'(\d+)', Rank[0])[0]
    except:
        BaiDuPCRank = ''

    try:
        BaiDuMobileRank = re.findall(r'(\d+)', Rank[1])[0]
    except:
        BaiDuMobileRank = ''

    try:
        SoGoPCRank = re.findall(r'(\d+)', Rank[2])[0]
    except:
        SoGoPCRank = ''

    try:
        BingPCRank = re.findall(r'(\d+)', Rank[3])[0]
    except:
        BingPCRank = ''

    try:
        So360PCRank = re.findall(r'(\d+)', Rank[4])[0][-1]
    except:
        So360PCRank = ''

    try:
        SMPCRank = re.findall(r'(\d+)', Rank[5])[0]
    except:
        SMPCRank = ''
        # print(BaiDuPCRank, BaiDuMobileRank, SoGoPCRank, BingPCRank, So360PCRank, SMPCRank)

    try:
        WordRank = re.findall(r'<i class="alexarank color-63">(.*?)</', source_code, re.S | re.M)[0]
    except:
        WordRank = ''
        # print('WordRank: ', WordRank)
    try:
        WebClassfiy = re.findall(r'网站分类：<i class="color-63">(.*?)</', source_code, re.S | re.M)[0]
    except:
        WebClassfiy = ''
    # print('WebClassfiy: ', WebClassfiy)

    # DomainRegister = re.findall(r'<a href="http://whois.chinaz.com/reverse\?ddlSearchMode=2&amp;host=(.*?)"',
    #                             source_code, re.S | re.M)[0]
    try:
        DomainRegister = re.findall(r'注册人/机构：(.*?)</', source_code, re.S | re.M)[0].replace(' ', '').split(',')
        DomainRegister = ''.join(DomainRegister[-2:])
        DomainRegister = ''.join(re.findall(r'[\u4e00-\u9fa5]', DomainRegister))
    except:
        DomainRegister = ''

    try:
        RegisterMail = re.findall(r'<a href="//whois.chinaz.com/reverse\?host=(.*?)&amp;',
                                  source_code, re.S | re.M)[0]
    except:
        RegisterMail = ''
    # print('RegisterMail: ', RegisterMail)

    try:
        DomainAges = re.findall(r'域名年龄：(.*?)</i',
                                source_code, re.S | re.M)[0].replace(' ', '').strip()

        DomainAge = DomainAges.split('>')[-1]
    except:
        DomainAge = ''
    # print('DomainAge: ', DomainAge)

    try:
        DomainRecord = re.findall(r'<span class="mr50">备案号：<i class="color-2f87c1">'
                                  r'<a href="//icp.chinaz.com/(.*?)</',
                                  source_code, re.S | re.M)[0]
        DomainRecord = DomainRecord.split('>')[-1]
    except:
        DomainRecord = ''
    # print('DomainRecord: ', DomainRecord)

    try:
        DomainName = re.findall(r'<a href="//data.chinaz.com/company/t0-p0-c0-i0-d0-s-(.*?)"',
                                source_code, re.S | re.M)[0]
    except:
        DomainName = ''
    # print('DomainName: ', DomainName)

    try:
        DomainGroup = re.findall(r'<span class="mr50">性质：<i class="color-63">(.*?)</',
                                 source_code, re.S | re.M)[0]
    except:
        DomainGroup = ''
    # print('DomainGroup: ', DomainGroup)

    try:
        DomainExamineTime = re.findall(r'<span>审核时间：<i class="color-63">(.*?)</',
                                       source_code, re.S | re.M)[0]
    except:
        DomainExamineTime = ''
    # print('DomainExamineTime: ', DomainExamineTime)

    try:
        DomainIp = re.findall(r'<a href="//ip.tool.chinaz.com/\?ip=(.*?)"',
                              source_code, re.S | re.M)[0]
    except:
        DomainIp = ''
    # print('DomainIp: ', DomainIp)

    try:
        SameIpDomain = re.findall(r'>同IP网站：<i class="color-63"><a href="//stool.chinaz.com/Same/\?s=(.*?)</i'.format(url),
                                  source_code, re.S | re.M)[0]
        SameIpDomainCount = SameIpDomain.split('>')
        SameIpDomain2 = SameIpDomainCount[-2].replace('/a', '').replace('<', '')
        SameIpDomain1 = SameIpDomainCount[-1]
        SameIpDomain = SameIpDomain2 + SameIpDomain1
        # print('SameIpDomain: ', SameIpDomain)
    except:
        SameIpDomain = 0

    try:
        DomainSpeed = re.findall(r'网站速度：<i class="color-63">'
                                 r'<a href="//ping.chinaz.com/\?host=(.*?)</i'.format(url),
                                 source_code, re.S | re.M)[0]
        DomainSpeeds = DomainSpeed.split('>')
        DomainSpeed1 = DomainSpeeds[-1]
        DomainSpeed2 = DomainSpeeds[-2].replace('/a', '').replace('<', '')
        DomainSpeed = DomainSpeed2 + DomainSpeed1
    except:

        DomainSpeed = ''

    # print('DomainSpeed: ', DomainSpeed)
    try:
        CompetitionDomain = re.findall(r'id="vie-total">(.*?)</'.format(url),
                                       source_code, re.S | re.M)[0]
        # print('CompetitionDomain: ', CompetitionDomain)
    except:

        CompetitionDomain = ''

    try:
        PCKWCount = re.findall(r'class="Ma01LiRow w10-7 pckwcount"><a href="//rank.chinaz.com/(.*?)</',
                               source_code, re.S | re.M)[0]
        PCKWCount = re.findall(r'\d+', PCKWCount)[0]
        # print('PCKWCount:', PCKWCount)
    except:
        PCKWCount = ''

    try:
        MoBileKWCount = re.findall(r'mobilekwcount">'
                                   r'<a href="//rank.chinaz.com/baidumobile/(.*?)</a',
                                   source_code, re.S | re.M)[0]
        MoBileKWCount = re.findall(r'\d+', MoBileKWCount)[0]
        # print('MoBileKWCount: ', MoBileKWCount)
    except:

        MoBileKWCount = ''

    try:
        FirstPageLocation = re.findall(r'id="seo_BaiduPagesLocation">'
                                       r'<a href="http://www.baidu.com/s\?wd=site%3A(.*?)</',
                                       source_code, re.S | re.M)[0]
        FirstPageLocation = re.findall(r'\d+', FirstPageLocation)[0]
    except:
        FirstPageLocation = ''
    # print('FirstPageLocation: ', FirstPageLocation)
    try:
        OutLinkCount = re.findall(r'outlinkcount"><a href="//outlink.chinaz.com/(.*?)</',
                                  source_code, re.S | re.M)[0]
        OutLinkCount = OutLinkCount.split('>')[-1]
        # print('OutLinkCount:', OutLinkCount)

    except:
        OutLinkCount = ''

    try:
        SeoSiteIndex = re.findall(r'id="seo_BaiduSiteIndex"><a href=(.*?)</',
                                  source_code, re.S | re.M)[0]
        SeoSiteIndex = SeoSiteIndex.split('>')[-1]
    except:
        SeoSiteIndex = ''
    # print('SeoSiteIndex: ', SeoSiteIndex)
    try:
        SeoBaiDuSiteIndex = re.findall(r'id="seo_BaiduSiteIndex_2"><a href=(.*?)</',
                                       source_code, re.S | re.M)[0]
        SeoBaiDuSiteIndex = SeoBaiDuSiteIndex.split('>')[-1]
    except:
        SeoBaiDuSiteIndex = ''
    # print('SeoBaiDuSiteIndex:', SeoBaiDuSiteIndex)
    try:
        OutBaiDuLink = re.findall(r'id="seo_BaiduLink">(.*?)</',
                                  source_code, re.S | re.M)[0]
        OutBaiDuLink = OutBaiDuLink.split('>')[-1]
        # print('OutBaiDuLink: ', OutBaiDuLink)
    except:
        OutBaiDuLink = ''

    try:
        SeoSoGoSiteIndex = re.findall(r'id="seo_SogouPages">'
                                      r'<a href="//www.sogou.com/web\?query=site%3A(.*?)</',
                                      source_code, re.S | re.M)[0]

        SeoSoGoSiteIndex = SeoSoGoSiteIndex.split('>')[-1]
    except:
        SeoSoGoSiteIndex = ''
    # print('SeoSoGoSiteIndex: ', SeoSoGoSiteIndex)

    try:
        OutSoGoLink = re.findall(r'<a href="//www.sogou.com/web\?query=www(.*?)</a',
                                 source_code, re.S | re.M)[0]
        OutSoGoLink = OutSoGoLink.split('>')[-1]
        # print('OutSoGoLink: ', OutSoGoLink)
    except:
        OutSoGoLink = ''

    try:
        Seo360SiteIndex = re.findall(r'id="seo_Pages360">'
                                     r'<a href="//www.so.com/s\?ie=utf-8&amp;fr=so.com&amp;src=home_www&amp;'
                                     r'nlpv=basesc&amp;q=site%3A(.*?)</',
                                     source_code, re.S | re.M)[0]
        Seo360SiteIndex = Seo360SiteIndex.split('>')[-1]
    except:
        Seo360SiteIndex = ''
    # print('Seo360SiteIndex: ', Seo360SiteIndex)

    try:
        Out360Link = re.findall(r'id="seo_Link360">'
                                r'<a href="//www.so.com/s\?ie=utf-8&amp;fr=so.com&amp;src=home_www&amp;'
                                r'nlpv=basesc&amp;q=domain%3A(.*?)</',
                                source_code, re.S | re.M)[0]
        Out360Link = Out360Link.split('>')[-1]
    except:
        Out360Link = ''
    # print('Out360Link: ', Out360Link)

    try:
        SeoBingSiteIndex = re.findall(r'id="seo_BingPages"><a href="http://cn.bing.com/search\?q=site%3A(.*?)</',
                                      source_code, re.S | re.M)[0]

        SeoBingSiteIndex = SeoBingSiteIndex.split('>')[-1]
    except:
        SeoBingSiteIndex = ''
    # print('SeoGoogleSiteIndex: ', SeoGoogleSiteIndex)


    try:
        OutBingLink = re.findall(r'id="seo_BingLink">(.*?)</',
                                   source_code, re.S | re.M)[0]
    except:
        OutBingLink = ''
    # print('OutGoogleLink: ', OutGoogleLink)


    try:
        SeoGoogleSiteIndex = re.findall(r'id="seo_GooglePages">'
                                        r'<a href="https://www.google.com/search\?q=site%3A(.*?)</',
                                        source_code, re.S | re.M)[0]

        SeoGoogleSiteIndex = SeoGoogleSiteIndex.split('>')[-1]
    except:
        SeoGoogleSiteIndex = ''
    # print('SeoGoogleSiteIndex: ', SeoGoogleSiteIndex)


    try:
        OutGoogleLink = re.findall(r'id="seo_GoogleLink">'
                                   r'<a href="https://www.google.com/search\?q=link%3A(.*?)</',
                                   source_code, re.S | re.M)[0]
        OutGoogleLink = OutGoogleLink.split('>')[-1]
    except:
        OutGoogleLink = ''
    # print('OutGoogleLink: ', OutGoogleLink)


    seo_dict_value = [
        website_name,
        AllFlowTotal, BaiDuPCRank, BaiDuMobileRank, SoGoPCRank,
        BingPCRank, So360PCRank, SMPCRank, WordRank, WebClassfiy,
        DomainRegister, RegisterMail, DomainAge, DomainRecord, DomainName,
        DomainGroup, DomainExamineTime, DomainIp, SameIpDomain, DomainSpeed,
        CompetitionDomain, PCKWCount, MoBileKWCount, FirstPageLocation,
        OutLinkCount, SeoSiteIndex, SeoBaiDuSiteIndex, OutBaiDuLink, SeoSoGoSiteIndex,
        OutSoGoLink, Seo360SiteIndex, Out360Link, SeoBingSiteIndex, OutBingLink,
        SeoGoogleSiteIndex, OutGoogleLink,
    ]

    seo_dict_content = dict(list(zip(seo_dict_name, seo_dict_value)))
    # print(seo_dict_content)
    # breakpoint()
    collect.insert_one(seo_dict_content)
    logging.warning('Seo Content {} Finished.'.format(DomainName))


def get_url():
    with open(r'website_url', encoding='utf-8') as f:
        url_lst = f.readlines()
    return url_lst


def main():
    collect = mongodb()
    website_urls = get_url()
    # website_urls = ['www.bast.net.cn']
    file_time = time.strftime("%Y-%m-%d", time.localtime(time.time()))
    # file_time = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d")

    for item in website_urls[1:]:
        # print(item)
        domain_name = item.split('#####')[0].strip()
        website_name = item.split('#####')[1].strip()
        # print(domain_name)
        # print(website_name)
        # breakpoint()
        url = 'https://seo.chinaz.com/{}'.format(domain_name)
        get_source_code(url, item)
        name = item.strip().replace('.', '-')
        with open(r'website-txt/{}-{}.txt'.format(name, file_time), encoding='utf-8') as f:
            source_code = f.readlines()
        logging.warning('Start {}-{} Get Content.'.format(name, file_time))
        source_code = str(source_code)
        get_weight(url, source_code, collect, website_name)
        time.sleep(0.5)


if __name__ == '__main__':
    main()
