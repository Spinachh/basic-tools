# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoole
# Date : 2022/11/18

import os
import time
import shutil
import pandas as pd
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class ImgClassFiy(object):

    def __init__(self, path, file_name, img_dir_name):
        self.path = path
        self.file_name = file_name
        self.img_dir_name = img_dir_name

    @staticmethod
    def img_data(img_path_close, img_path_far, img_path_special, source_path, data):
        cnt_row = 0
        # for index, row in data.iterrows():      # index row number
        for index, row in data.iterrows():      # index row number
            cnt_row += 1                        # row increase
            row_name = row['name']              # first col name
            prob_num = row['number']
            logging.warning('ctnNum: {} close: {} far: {} special: {}'
                            .format(cnt_row,
                                    row['close'],
                                    row['far'],
                                    row['special']))  # 3379227

            with open(r'sys-name.txt', 'a', encoding='utf-8') as f:
                f.write('row: {} name: {} prob_num: {} close: {} far:{} special:{} '
                        .format(cnt_row, row['name'], row['number'],
                                row['close'], row['far'], row['special']) + '\n')

            if prob_num != 'nan':

                if 'jpg' in str(row['close']):
                    row_close_text = str(row['close']).replace(' ', '').\
                        replace('.jpg', '_{}.jpg'.format(prob_num))      # second col data
                else:
                    if str(row['close']) != '0':
                        row_close_text = str(row['close']).replace(' ', '')\
                                         + '_{}'.format(prob_num) + '.jpg'
                    else:
                        row_close_text = str(row['close'])  # second col data

                if 'jpg' in str(row['far']):
                    row_far_text = str(row['far']).replace(' ', '').\
                        replace('.jpg', '_{}.jpg'.format(prob_num))
                    # third col data
                else:
                    if str(row['far']) != '0':
                        row_far_text = str(row['far']).replace(' ', '')\
                                       + '_{}'.format(prob_num) + '.jpg'
                        # third col data
                    else:
                        row_far_text = str(row['far'])  # third col data

                if 'jpg' in str(row['special']):
                    row_special_text = str(row['special']).replace(' ', '').\
                        replace('.jpg', '_{}.jpg'.format(prob_num))
                    # fifth col data and Upper it
                else:
                    if str(row['special']) != '0':
                        row_special_text = str(row['special']).replace(' ', '')\
                                           + '_{}'.format(prob_num) + '.jpg'
                        # fifth col data and Upper it
                    else:
                        row_special_text = str(row['special'])  # fifth col data and Upper it

            else:

                if 'jpg' in str(row['close']):
                    row_close_text = str(row['close']).replace(' ', '')  # second col data
                else:
                    if str(row['close']) != '0':
                        row_close_text = str(row['close']) + '.jpg'
                    else:
                        row_close_text = str(row['close'])  # second col data

                if 'jpg' in str(row['far']):
                    row_far_text = str(row['far']).replace(' ', '')
                else:
                    if str(row['far']) != '0':
                        row_far_text = str(row['far']) + '.jpg'
                    else:
                        row_far_text = str(row['far'])  # third col data

                if 'jpg' in str(row['special']):
                    row_special_text = str(row['special']).replace(' ', '')
                else:
                    if str(row['special']) != '0':
                        row_special_text = str(row['special']) + '.jpg'
                    else:
                        row_special_text = str(row['special'])

            file_close_path = img_path_close + '\\' + row_name   # close path
            file_far_path = img_path_far + '\\' + row_name       # far path
            file_special_path = img_path_special + '\\' + row_name    # special path

            if os.path.exists(file_close_path):
                if row_close_text != '0':
                    if not os.path.exists(file_close_path + '\\' + row_close_text):
                        if os.path.exists(source_path + '\\' + row_close_text):  # source directory has file to copy
                            try:
                                shutil.copy(source_path + '\\' + row_close_text, file_close_path)
                            except:
                                logging.warning('source_path has no {} img'
                                                .format(row_close_text))
                        else:
                            with open(r'sys-close-error.txt', 'a', encoding='utf-8') as f:
                                f.write(source_path + '\\' + row_close_text + '\n')
            else:
                os.makedirs(file_close_path)
                if row_close_text != '0':
                    try:
                        shutil.copy(source_path + '\\' + row_close_text, file_close_path)
                    except:
                        logging.warning('source_path has no {} img'.format(row_close_text))

            if os.path.exists(file_far_path):
                if row_far_text != '0':
                    if not os.path.exists(file_far_path + '\\' + row_far_text):
                        if os.path.exists(source_path + '\\' + row_far_text):
                            try:
                                shutil.copy(source_path + '\\' + row_far_text, file_far_path)
                            except:
                                logging.warning('source_path has no {} img'
                                                .format(row_far_text))
                        else:
                            with open(r'sys-far-error.txt', 'a', encoding='utf-8') as f:
                                f.write(source_path + '\\' + row_close_text + '\n')

            else:
                os.makedirs(file_far_path)
                if row_far_text != '0':
                    try:
                        shutil.copy(source_path + '\\' + row_far_text, file_far_path)
                    except:
                        logging.warning('source_path has no {} img'.format(row_far_text))

            if os.path.exists(file_special_path):
                if row_special_text != '0':
                    if not os.path.exists(file_special_path + '\\' + row_special_text):
                        if os.path.exists(source_path + '\\' + row_special_text):
                            try:
                                shutil.copy(source_path + '\\' + row_special_text, file_special_path)
                            except:
                                logging.warning('source_path has no {} img'
                                                .format(row_special_text))
                        else:
                            with open(r'sys-special-error.txt', 'a', encoding='utf-8') as f:
                                f.write(source_path + '\\' + row_close_text + '\n')

            else:
                os.makedirs(file_special_path)
                if row_special_text != '0':
                    try:
                        shutil.copy(source_path + '\\' + row_special_text, file_special_path)
                    except:
                        logging.warning('source_path has no {} img'.format(row_special_text))

            logging.warning('Row:{} Name:{} Prob:{} Was Finished!'
                            .format(cnt_row, row_name, prob_num))


    def start_run(self):

        start_time = time.time()
        # img_path = self.path + '\\' + self.img_dir_name + '-IMG-CLASS'     # img final directory
        # img_path_close = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' +
        # '\\' + 'close'     # img final directory
        img_path_close = self.path + '\\' + 'SYS-Result' + '-IMG-CLASS' +\
                         '\\' + 'close'     # img final directory
        # img_path_far = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' + '\\' + 'far'     # img final directory
        img_path_far = self.path + '\\' + 'SYS-Result' + '-IMG-CLASS' + '\\' + \
                       'far'     # img final directory
        # img_path_special = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' +
        # '\\' + 'special'    # img final directory
        img_path_special = self.path + '\\' + 'SYS-Result' + '-IMG-CLASS' + '\\' \
                           + 'special'    # img final directory

        # if not os.path.exists(img_path):
        #     os.mkdir(img_path)
        if not os.path.exists(img_path_close):
            os.makedirs(img_path_close)
        if not os.path.exists(img_path_far):
            os.makedirs(img_path_far)
        if not os.path.exists(img_path_special):
            os.makedirs(img_path_special)

        source_path = os.path.join(self.path, self.img_dir_name)
        ori = pd.read_excel(io=self.path + '\\' + self.file_name)
        # pandas read xlsx file
        data = ori.iloc[0:, 0:5]  # [row, col]
        # row ,col get data (0-row,0~6col)
        # print(data)
        # breakpoint()
        self.img_data(img_path_close, img_path_far, img_path_special, source_path, data)
        end_time = time.time()
        total_time = end_time - start_time
        logging.warning('Data Has Total Time :{} Seconds'.format(total_time))


if __name__ == '__main__':

    path = r'G:\mongoole'      # data result directory
    file_name = 'sys-result-11-11.xlsx'     # xlsx file name
    img_dir_name = 'SYS-Data-Target'        # img directory name
    img_class = ImgClassFiy(path, file_name, img_dir_name)
    img_class.start_run()
