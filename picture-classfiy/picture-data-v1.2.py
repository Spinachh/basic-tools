# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/8/19 

import os
import shutil
import pandas as pd
import logging

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def picture_data(picture_path, source_path, data):
    cnt_row = 0
    for index, row in data.iterrows():
        cnt_row += 1
        row_name = row['name']
        row_close_text = str(row['close']).upper()
        row_far_text = str(row['far']).upper()
        row_special_text = str(row['special']).upper()

        file_close_path = picture_path + '\\' + row_name + '\\' + 'close'
        file_far_path = picture_path + '\\' + row_name + '\\' + 'far'
        file_special_path = picture_path + '\\' + row_name + '\\' + 'special'

        if os.path.exists(file_close_path):
            if row_close_text != 'NAN':
                if os.path.exists(file_close_path + '\\' + row_close_text):
                    pass
                else:
                    if os.path.exists(source_path + '\\' + row_close_text):
                        shutil.move(source_path + '\\' + row_close_text, file_close_path)
        else:
            os.makedirs(file_close_path)
            if row_close_text != 'NAN':
                shutil.move(source_path + '\\' + row_close_text, file_close_path)

        if os.path.exists(file_far_path):
            if row_far_text != 'NAN':
                if os.path.exists(file_far_path + '\\' + row_far_text):
                    pass
                else:
                    if os.path.exists(source_path + '\\' + row_far_text):
                        shutil.move(source_path + '\\' + row_far_text, file_far_path)
        else:
            os.makedirs(file_far_path)
            if row_far_text != 'NAN':
                shutil.move(source_path + '\\' + row_far_text, file_far_path)

        if os.path.exists(file_special_path):
            if row_special_text != 'NAN':
                if os.path.exists(file_special_path + '\\' + row_special_text):
                    pass
                else:
                    if os.path.exists(source_path + '\\' + row_special_text):
                        shutil.move(source_path + '\\' + row_special_text, file_special_path)
        else:
            os.makedirs(file_special_path)
            if row_special_text != 'NAN':
                shutil.move(source_path + '\\' + row_special_text, file_special_path)

        logging.warning('The Number Row :{} Was Finished!'.format(cnt_row))


def main():
    path = r'D:\data-test'
    picture_path = path + '\\' + 'picture-classfiy'
    source_path = path + '\\' + 'SJY0725'
    # dist_path = path + '\\'
    # file = 'test'
    ori = pd.read_excel(io=path + '\\' + 'test.xlsx')
    data = ori.iloc[0:, 0:6]  # [row, col]

    picture_data(picture_path, source_path, data)


if __name__ == '__main__':
    main()
