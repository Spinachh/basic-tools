# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoole
# Date : 2022/8/19 

import os
import time
import shutil
import pandas as pd
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class ImgClassFiy(object):

    def __init__(self, path, file_name, img_dir_name):
        self.path = path
        self.file_name = file_name
        self.img_dir_name = img_dir_name

    @staticmethod
    def img_data(img_path_close, img_path_far, img_path_special, source_path, data):
        cnt_row = 0
        for index, row in data.iterrows():      # index row number
            cnt_row += 1                        # row increase
            row_name = row['name']              # first col name
            row_close_text = str(row['close']).upper()      # second col data
            row_far_text = str(row['far']).upper()          # third col data
            row_special_text = str(row['special']).upper()  # fifth col data and Upper it

            file_close_path = img_path_close + '\\' + row_name   # close path
            file_far_path = img_path_far + '\\' + row_name       # far path
            file_special_path = img_path_special + '\\' + row_name    # special path

            if os.path.exists(file_close_path):
                if row_close_text != 'NAN':
                    if not os.path.exists(file_close_path + '\\' + row_close_text):
                        if os.path.exists(source_path + '\\' + row_close_text):  # source directory has file to copy
                            shutil.copy(source_path + '\\' + row_close_text, file_close_path)
            else:
                os.makedirs(file_close_path)
                if row_close_text != 'NAN':
                    shutil.copy(source_path + '\\' + row_close_text, file_close_path)

            if os.path.exists(file_far_path):
                if row_far_text != 'NAN':
                    if not os.path.exists(file_far_path + '\\' + row_far_text):
                        if os.path.exists(source_path + '\\' + row_far_text):
                            shutil.copy(source_path + '\\' + row_far_text, file_far_path)

            else:
                os.makedirs(file_far_path)
                if row_far_text != 'NAN':
                    shutil.copy(source_path + '\\' + row_far_text, file_far_path)

            if os.path.exists(file_special_path):
                if row_special_text != 'NAN':
                    if not os.path.exists(file_special_path + '\\' + row_special_text):
                        if os.path.exists(source_path + '\\' + row_special_text):
                            shutil.copy(source_path + '\\' + row_special_text, file_special_path)
            else:
                os.makedirs(file_special_path)
                if row_special_text != 'NAN':
                    shutil.copy(source_path + '\\' + row_special_text, file_special_path)

            logging.warning('The Number Row :{} Was Finished!'.format(cnt_row))

    def start_run(self):
        start_time = time.time()
        # img_path = self.path + '\\' + self.img_dir_name + '-IMG-CLASS'     # img final directory
        img_path_close = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' + '\\' + 'close'     # img final directory
        img_path_far = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' + '\\' + 'far'     # img final directory
        img_path_special = self.path + '\\' + self.img_dir_name + '-IMG-CLASS' + '\\' + 'special'     # img final directory

        # if not os.path.exists(img_path):
        #     os.mkdir(img_path)
        if not os.path.exists(img_path_close):
            os.makedirs(img_path_close)
        if not os.path.exists(img_path_far):
            os.makedirs(img_path_far)
        if not os.path.exists(img_path_special):
            os.makedirs(img_path_special)

        source_path = os.path.join(self.path, self.img_dir_name)
        ori = pd.read_excel(io=self.path + '\\' + self.file_name)   # pandas read xlsx file
        data = ori.iloc[0:, 0:4]  # [row, col]              # row ,col get data (0-row,0~6col)
        self.img_data(img_path_close, img_path_far, img_path_special, source_path, data)
        end_time = time.time()
        total_time = end_time - start_time
        logging.warning('Data Has Total Time :{} Seconds'.format(total_time))


if __name__ == '__main__':
    path = r'D:\data-test'      # data result directory
    file_name = 'test.xlsx'     # xlsx file name
    img_dir_name = 'SJY0725'        # img directory name
    img_class = ImgClassFiy(path, file_name, img_dir_name)
    img_class.start_run()
