# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoole
# Date : 2022/02/03

import os
import time
import shutil
import pandas as pd
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
file_time = time.strftime("%Y-%m-%d", time.localtime())


class ImgClassFiy(object):

    def __init__(self, path, file_name, img_dir_name):
        self.path = path
        self.file_name = file_name
        self.img_dir_name = img_dir_name

    @staticmethod
    def img_data(img_path_close, img_path_far, img_path_special, source_path, data):
        error_line = ['nan']
        cnt_row = 0
        for index, row in data.iterrows():      # index row number
            cnt_row += 1                        # row increase
            row_name = row['name']              # first col name
            # print(str(row['close']))
            # print(str(row['far']))
            # print(str(row['special']))
            # breakpoint()
            if str(row['close']) not in error_line:
                row_close_text = str(row['close']) + '.jpg'
            else:
                row_close_text = str(row['close'])  # second col data

            if str(row['far']) not in error_line:
                row_far_text = str(row['far']) + '.jpg'
            else:
                row_far_text = str(row['far'])  # third col data

            if str(row['special']) not in error_line:
                row_special_text = str(row['special']) + '.jpg'
            else:
                row_special_text = str(row['special'])  # fifth col data and Upper it

            # img_path_close = r'G:\mongoole\Result2-IMG-CLASS\close'
            file_close_path = img_path_close + '\\' + row_name   # close path
            file_far_path = img_path_far + '\\' + row_name       # far path
            file_special_path = img_path_special + '\\' + row_name    # special path
            logging.warning('name: {} close:{} far:{} sp:{}'.format(
                row_name, row_close_text, row_far_text, row_special_text
            ))
            # print(file_close_path)
            # print(source_path + '\\' + row_close_text)
            # breakpoint()
            # source_path = r'G:\mongoole\Data-Target'
            print(source_path + '\\' + row_close_text)
            try:
                shutil.copy(source_path + '\\'
                            + row_close_text,
                            file_close_path)
            except:
                logging.warning('source_path has no {} img'
                                .format(row_close_text))
            if os.path.exists(img_path_far):
                if row_far_text not in error_line:
                    try:
                        shutil.copy(source_path + '\\' +
                                    row_far_text,
                                    file_far_path)
                    except:
                        logging.warning('source_path has no {} img'
                                        .format(row_far_text))

            if os.path.exists(img_path_special):
                if row_special_text not in error_line:
                    try:
                        shutil.copy(source_path + '\\'
                                    + row_special_text,
                                    file_special_path)
                    except:
                        logging.warning('source_path has no {} img'
                                        .format(row_special_text))
            logging.warning('Row:{} Name:{} Was Finished!'
                            .format(cnt_row, row_name))

    def start_run(self):

        start_time = time.time()
        img_path_close = self.path + '\\' + 'Result-2023' + '-IMG-CLASS' +\
                         '\\' + 'close'     # img final directory
        img_path_far = self.path + '\\' + 'Result-2023' + '-IMG-CLASS' + '\\' + \
                       'far'     # img final directory

        img_path_special = self.path + '\\' + 'Result-2023' + '-IMG-CLASS' + '\\' \
                           + 'special'    # img final directory

        if not os.path.exists(img_path_close):
            os.makedirs(img_path_close)
        if not os.path.exists(img_path_far):
            os.makedirs(img_path_far)
        if not os.path.exists(img_path_special):
            os.makedirs(img_path_special)

        source_path = os.path.join(self.path, self.img_dir_name)
        # r'G:\mongoole\Data-Target'
        ori = pd.read_excel(io=self.path + '\\' + self.file_name)   # pandas read xlsx file
        data = ori.iloc[0:, 0:4]  # [row, col]   # row ,col get data (0-row,0~6col)
        # print(data)
        # breakpoint()
        self.img_data(img_path_close, img_path_far, img_path_special, source_path, data)
        end_time = time.time()
        total_time = end_time - start_time
        logging.warning('Data Has Total Time :{} Seconds'.format(total_time))


if __name__ == '__main__':

    path = r'G:\mongoole'      # data result directory
    file_name = '2023-02-03-add.xlsx'     # xlsx file name
    img_dir_name = 'Data-Target-2023'        # img directory name
    img_class = ImgClassFiy(path, file_name, img_dir_name)
    img_class.start_run()
