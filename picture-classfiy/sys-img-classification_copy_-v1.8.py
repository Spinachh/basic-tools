# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoole
# Date : 2022/11/28

import os
import time
import shutil
import pandas as pd
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
file_time = time.strftime("%Y-%m-%d", time.localtime())


class ImgClassFiy(object):

    def __init__(self, path, file_name, img_dir_name):
        self.path = path
        self.file_name = file_name
        self.img_dir_name = img_dir_name

    @staticmethod
    def img_data(img_path_close, img_path_far, img_path_special, source_path, data):
        error_line = ['0', 'nan']
        cnt_row = 0
        # for index, row in data.iterrows():      # index row number
        for index, row in data.iterrows():      # index row number
            cnt_row += 1                        # row increase
            row_name = row['name']              # first col name
            prob_num = row['number']
            logging.warning('ctnNum: {} close: {} far: {} special: {}'
                            .format(cnt_row,
                                    row['close'],
                                    row['far'],
                                    row['special']))  # 3379227

            if str(row['close']) not in error_line:
                row_close_text = str(row['close']) + '.jpg'
            else:
                row_close_text = str(row['close'])  # second col data

            if str(row['far']) not in error_line:
                row_far_text = str(row['far']) + '.jpg'
            else:
                row_far_text = str(row['far'])  # third col data

            if str(row['special']) not in error_line:
                row_special_text = str(row['special']) + '.jpg'
            else:
                row_special_text = str(row['special'])  # fifth col data and Upper it

            file_close_path = img_path_close + '\\' + row_name   # close path
            file_far_path = img_path_far + '\\' + row_name       # far path
            file_special_path = img_path_special + '\\' + row_name    # special path

            if os.path.exists(file_close_path):     # 电力井盖文件夹
                if row_close_text not in error_line:       # excel第N行的close值

                    if os.path.exists(source_path + '\\'
                                      + row_close_text):  # 图片库里该close值存在
                        try:
                            shutil.copy(source_path + '\\'
                                        + row_close_text,
                                        file_close_path)  # 从图片库复制图片到目的文件夹
                        except:
                            with open(r'sys\sys-img-copy-close-error-{}'
                                      r'.txt'.format(file_time),
                                      'a',
                                      encoding='utf-8'
                                      ) as f:  # 图片库复制图片报错日志文件

                                f.write(source_path + '\\' + row_close_text + '\n')
                            logging.warning('source_path has no {} img'
                                            .format(row_close_text))

                    else:   # 图片库里该close值不存在
                        with open(r'sys\sys-no-img-close-error-{}'
                                  r'.txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:  # 图片库复制图片报错日志文件
                            f.write(source_path + '\\' + row_close_text + '\n')

            else:
                os.makedirs(file_close_path)    # 创建电力井盖文件夹
                if row_close_text not in error_line:   # close值不为0
                    try:
                        shutil.copy(source_path + '\\' +
                                    row_close_text, file_close_path)
                    except:
                        with open(r'sys\sys-img-copy-close-error-{}'
                                  r'.txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:   # 图片库复制图片报错日志文件
                            f.write(source_path + '\\' +
                                    row_close_text + '-mkdir' + '\n')

            if os.path.exists(file_far_path):   # 电力井盖文件夹
                if row_far_text not in error_line:     # excel第N行的close值

                    if os.path.exists(source_path + '\\'
                                      + row_far_text):  # 图片库里该close值存在
                        try:
                            shutil.copy(source_path + '\\'
                                        + row_far_text,
                                        file_far_path)  # 从图片库复制图片到目的文件夹
                        except:
                            with open(r'sys\sys-img-copy-far-error-{}'
                                      r'.txt'.format(file_time),
                                      'a',
                                      encoding='utf-8'
                                      ) as f:  # 图片库复制图片报错日志文件
                                f.write(source_path + '\\' + row_close_text + '\n')
                            logging.warning('source_path has no {} img'
                                            .format(row_far_text))

                    else:   # 图片库里该far值不存在
                        with open(r'sys\sys-no-img-far-error-{}'
                                  r'.txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:  # 图片库复制图片报错日志文件
                            f.write(source_path + '\\' + row_far_text + '\n')

            else:
                os.makedirs(file_far_path)
                if row_far_text not in error_line:
                    try:
                        shutil.copy(source_path + '\\' +
                                    row_far_text, file_far_path)
                    except:
                        with open(r'sys\sys-img-copy-far-error-{}.'
                                  r'txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:
                            f.write(source_path + '\\' +
                                    row_close_text + '-mkdir' + '\n')
                        logging.warning('source_path has no {} img'.
                                        format(row_far_text))

            if os.path.exists(file_special_path):
                if row_special_text not in error_line:

                    if os.path.exists(source_path + '\\'
                                      + row_special_text):
                        try:
                            shutil.copy(source_path + '\\'
                                        + row_special_text,
                                        file_special_path)
                        except:
                            with open(r'sys\sys-img-copy-special-error-{}.'
                                      r'txt'.format(file_time),
                                      'a',
                                      encoding='utf-8'
                                      ) as f:
                                f.write(source_path + '\\' + row_close_text + '\n')
                            logging.warning('source_path has no {} img'
                                            .format(row_special_text))

                    else:   # 图片库里该special值不存在
                        with open(r'sys\sys-no-img-special-error-{}'
                                  r'.txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:  # 图片库复制图片报错日志文件
                            f.write(source_path + '\\' + row_far_text + '\n')

            else:
                os.makedirs(file_special_path)
                if row_special_text not in error_line:
                    try:
                        shutil.copy(source_path + '\\' +
                                    row_special_text,
                                    file_special_path)
                    except:
                        with open(r'sys\sys-img-copy-special-error-{}.'
                                  r'txt'.format(file_time),
                                  'a',
                                  encoding='utf-8'
                                  ) as f:
                            f.write(source_path + '\\' +
                                    row_close_text + '-mkdir' + '\n')
                        logging.warning('source_path has no {} img'.
                                        format(row_special_text))

            logging.warning('Row:{} Name:{} Prob:{} Was Finished!'
                            .format(cnt_row, row_name, prob_num))


    def start_run(self):

        start_time = time.time()
        img_path_close = self.path + '\\' + 'SYS2-Result' + '-IMG-CLASS' +\
                         '\\' + 'close'     # img final directory
        img_path_far = self.path + '\\' + 'SYS2-Result' + '-IMG-CLASS' + \
                       '\\' + 'far'     # img final directory

        img_path_special = self.path + '\\' + 'SYS2-Result' + '-IMG-CLASS'\
                           + '\\' + 'special'    # img final directory

        if not os.path.exists(img_path_close):
            os.makedirs(img_path_close)
        if not os.path.exists(img_path_far):
            os.makedirs(img_path_far)
        if not os.path.exists(img_path_special):
            os.makedirs(img_path_special)

        source_path = os.path.join(self.path, self.img_dir_name)
        ori = pd.read_excel(io=self.path + '\\' + self.file_name)
        # pandas read xlsx file
        data = ori.iloc[0:, 0:5]  # [row, col]
        # row ,col get data (0-row,0~6col)
        # print(data)
        # breakpoint()
        self.img_data(img_path_close, img_path_far, img_path_special, source_path, data)
        end_time = time.time()
        total_time = end_time - start_time
        logging.warning('Data Has Total Time :{} Seconds'.format(total_time))


if __name__ == '__main__':

    path = r'G:\mongoole'      # data result directory
    file_name = 'sys-result-11-27.xlsx'     # xlsx file name
    img_dir_name = 'SYS-Data-Target'        # img directory name
    img_class = ImgClassFiy(path, file_name, img_dir_name)
    img_class.start_run()
