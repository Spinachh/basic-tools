# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/11/28


import os
import time
import logging

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
file_time = time.strftime("%Y-%m-%d", time.localtime())

img_dir_name = 'G:\\mongoole\\Data-Target\\'  # img directory name
# img_dir_name = 'I:\\gfc\\修改名称后的照片\\'  # img directory name
imglist = os.listdir(img_dir_name)
# DX-ZXF0820 (589)到DX-ZXF0820 (1329)
# if img ==
for name in range(589, 1330):
    file_name = 'DX-ZXF0820' + ' ({})'.format(str(name)) + '.jpg'
    path_file_name = img_dir_name + '\\' + file_name
    if os.path.exists(path_file_name):
        os.remove(path_file_name)
        print('成功删除文件:', path_file_name)
    else:
        print('未找到此文件:', path_file_name)
