# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/11/11

import os
import shutil
import logging


logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


target_path = r"D:\data-test\Data-Target-道路交通设施"
source_path = r"D:\data-test\道路交通设施"
if not os.path.exists(target_path):
    os.makedirs(target_path)

for root, dirs, files in os.walk(source_path):
    for name in files:

        deep_root_path_file = os.path.join(root, name)
        deep_root_path_name = os.path.join(root, name).split('\\')
        img_name = '-'.join(deep_root_path_name[-3::])
        logging.warning('This File Name :{}'.format(name))
        target_path_file = os.path.join(target_path, img_name)
        if not os.path.exists(target_path_file):
            shutil.copy(deep_root_path_file, target_path + '\\' + img_name) # 复制文件的时候并重命名
        else:
            logging.warning('This Img Has Exist：{}!'.format(name))