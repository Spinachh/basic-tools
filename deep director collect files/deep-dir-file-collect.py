# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/8/22

import os
import shutil
import logging


logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


target_path = r"D:\data-test\Data-Target"
source_path = r"D:\data-test\SJY0725-IMG-CLASS"
if not os.path.exists(target_path):
    os.makedirs(target_path)

for root, dirs, files in os.walk(source_path):
    for name in files:
        # print(os.path.join(root, name))
        deep_root_path_file = os.path.join(root, name)
        logging.warning('This File Name :{}'.format(name))
        target_path_file = os.path.join(target_path,name)
        if not os.path.exists(target_path_file):
            shutil.move(deep_root_path_file, target_path)
        else:
            logging.warning('This Img Has Exist：{}!'.format(name))