# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/11/11

import os
import re
import shutil
import logging


logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


'''
source_dir_names_sys = ["7.4-7.17", "7.18-8.1", "7月4日前（压缩后）",
                        "8.1-8.14", "8.15-8.27", "10月3日0点-10月6日0点",
                        "0830-0907早上6点", "0907早上6点-0913早上6点1910条",
                        "0913早上6点-0918晚上8点2791个部件",
                        "0918晚上8点-0927凌晨0点-995条",
                        "地理编码", "牌匾标识", "天宫院街道-0727凌晨0点-10月3号凌晨0点"
                        ]
'''
source_dir_names_sys = ["7.18-8.1", "7月4日前（压缩后）",
                        "8.1-8.14", "8.15-8.27", "10月3日0点-10月6日0点",
                        "0830-0907早上6点", "0907早上6点-0913早上6点1910条",
                        "0913早上6点-0918晚上8点2791个部件",
                        "0918晚上8点-0927凌晨0点-995条",
                        "地理编码", "牌匾标识", "天宫院街道-0727凌晨0点-10月3号凌晨0点"
                        ]

# source_dir_names = ["2生物医药基地补测0919-270个左右",
#                     "07.14行道树-兴业大街（双高路-金星西路）",
#                     "9.4行道树", "0903滨河公园", "行道树2022.9.17",
#                     "康凯", "康凯-1597个", "康凯补测09.21-09.23-1151个",
#                     "康凯李洋补测-1486个", "李檀-excel无统计",
#                     "李洋", "李洋补测20220921-22-23-1075个",
#                     "李洋李杰-1187个", "陆泽宣", "陆泽宣0906", "孟庆傲",
#                     "生物医药基地补测0919-1504个", "生物医药基地第二批",
#                     "生物医药基地第三批", "生物医药基地第四批",
#                     "生物医药基地绿地-825个", "生物医药基地线性数据",
#                     "树及护树设施1437个", "宋工照片", "宋乐章",
#                     "新增绿化", "王子俊", "修改名称后的照片",
#                     "张昕宇", "赵阔+护树设施-1066个", "赵阔组"
#                     ]

for dir_name in source_dir_names_sys:
    target_path_sys = r"G:\mongoole\SYS-Data-Target"
    source_path = r"H:\gfc\{}".format(dir_name)
    if not os.path.exists(target_path_sys):
        os.makedirs(target_path_sys)

    for root, dirs, files in os.walk(source_path):
        for name in files:
            deep_root_path_file = os.path.join(root, name)
            deep_root_path_name = os.path.join(root, name).split('\\')
            pro_name = os.path.join(root, name).split('\\')[-2]
            # part_name1 = os.path.join(root, name).split('\\')[-3]
            # part_name = re.findall(r'\d+', part_name1)[0]
            # print(deep_root_path_name)
            img_namee = os.path.join(name).replace('.jpg', '_{}.jpg'
                                                   .format(pro_name))
            # print(img_namee)
            # breakpoint()
            # img_name = '-'.join(deep_root_path_name[-3::])
            logging.warning('DirName: {}  File Name :{}'.format(dir_name, img_namee))
            target_path_file = os.path.join(target_path_sys, img_namee)
            if not os.path.exists(target_path_file):
                if '.txt' not in deep_root_path_file:
                    shutil.copy(deep_root_path_file,
                                target_path_sys + '\\' + img_namee)
                    # 复制文件的时候并重命名
            else:
                if dir_name != "7.18-8.1":
                    with open(r'sys-img-error.txt', 'a', encoding='utf-8') as f:
                        f.write(deep_root_path_file + '\n')
                    logging.warning('DirName: {}   Img Has Exist：{}!'.
                                    format(dir_name, name))
                else:
                    logging.warning('DirName: {}   Img Has Exist：{}!'.
                                    format(dir_name, name))