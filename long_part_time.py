# -*-coding:utf-8 -*-
# Author: mongoole
# Date: 2023-04-14


import datetime
import pandas as pd


def get_publish_date(data, time_interval):
    # 获取列名，三种方法
    columns_names = [column for column in data]
    # columns_names1 = ori.columns.values
    # columns_names2 = ori.columns.tolist

    for index, row in data.iterrows():
        time_lst = []
        for col_index in range(len(columns_names)):  # 循环列名获取列的值
            if row[columns_names[col_index]] != 0:
                item = '2023-' + columns_names[col_index].replace('月', '-').replace('日', '')
                tweet_date = datetime.datetime.strptime(item, "%Y-%m-%d")
                time_lst.append(tweet_date)
            else:
                pass
        max_time(time_interval, time_lst)


def max_time(time_interval, time_lst):
    if len(time_lst) == 0:
        print("三个月内的最大发文时间间隔超过90天。")

    elif len(time_lst) < 2:
        a = time_lst[0] - datetime.datetime.strptime('2023-01-01', "%Y-%m-%d")
        b = datetime.datetime.strptime('2023-03-31', "%Y-%m-%d") - time_lst[0]
        if a > b:
            max_time_difference = a
        else:
            max_time_difference = b
        print("三个月内的最大发文时间间隔为：", max_time_difference)

    elif len(time_lst) == 2:
        a = time_lst[0] - datetime.datetime.strptime('2023-01-01', "%Y-%m-%d")
        b = time_lst[1] - time_lst[0]
        c = datetime.datetime.strptime('2023-03-31', "%Y-%m-%d") - time_lst[1]
        max_time_difference = [a, b, c]
        max_time_difference.sort()
        max_time_difference = max_time_difference[-1]
        print("三个月内的最大发文时间间隔为：", max_time_difference)
    else:
        time_lst.sort()  # 将发文时间列表按照时间顺序排序
        if time_lst[0] != datetime.datetime.strptime('2023-01-01', "%Y-%m-%d"):
            max_time_difference = time_lst[0] - datetime.datetime.strptime('2023-01-01', "%Y-%m-%d")
            loop_value(time_lst, max_time_difference, time_interval)
        elif time_lst[-1] != datetime.datetime.strptime('2023-03-31', "%Y-%m-%d"):
            max_time_difference = datetime.datetime.strptime('2023-01-01', "%Y-%m-%d") - time_lst[-1]
            loop_value(time_lst, max_time_difference, time_interval)
        else:
            max_time_difference = datetime.timedelta(days=0)
            loop_value(time_lst, max_time_difference, time_interval)


def loop_value(time_lst, max_time_difference, time_interval):
    for i in range(len(time_lst) - 1):
        time_difference = time_lst[i + 1] - time_lst[i]
        if time_difference > max_time_difference:
            max_time_difference = time_difference

    if max_time_difference >= time_interval:
        print("三个月内的最大发文时间间隔超过90天。")
    else:
        print("三个月内的最大发文时间间隔为：", max_time_difference)


def main():
    path = r'E:\kexiewebsite'
    file_name = r'long_time.xlsx'
    time_interval = datetime.timedelta(days=90)
    ori = pd.read_excel(io=path + '\\' + file_name)  # pandas read xlsx file
    data = ori.iloc[0:635, 2:91]  # 元素1表示取行的区间值，元素2表示取列的区间值
    get_publish_date(data, time_interval)


if __name__ == '__main__':
    main()
