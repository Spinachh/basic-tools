#coding:utf-8

from openpyxl import load_workbook

#大写的金额转换
def num_to_chn(bank_money):
    if not isinstance(bank_money, float) and not isinstance(bank_money, int):
        return 'non bank_money'
    if bank_money < 0 or bank_money > 9999999999999.99:
        return 'wrong bank_money'
    if bank_money == 0:
        return '零元'
    c_d = {'0': '零', '1': '壹', '2': '贰', '3': '叁', '4': '肆', '5': '伍', '6': '陆', '7': '柒', '8': '捌', '9': '玖'}
    d_d = {0: '分', 1: '角', 2: '元', 3: '拾', 4: '佰', 5: '仟', 6: '万', 7: '拾', 8: '佰', 9: '仟', 10: '亿', 11: '拾', 12: '佰',
           13: '仟',
           14: '万'}
    L = []
    pre = '0'
    s = str(int(bank_money * 100))[::-1].replace('.', '')
    index = -1

    for c in s:
        index += 1
        if c == '0' and pre == '0':
            if index == 2:
                L.insert(0, '元')
        elif c == '0':
            if index == 2:
                L.insert(0, '元')
            else:
                L.insert(0, '零')
            pre = c
        else:
            L.insert(0, c_d[c] + "" + d_d[index])
            pre = c
    return ''.join(L)

##读取

wb = load_workbook('银行进账单数据录入.xlsx')
#获取sheet
sheet = wb.active

#获取行数
row_num = sheet.max_row - 2
# print(row_num)

for i in range(row_num):
    #序号
    bank_serial = sheet['A' + str(3 + i)].value
    # print(sheet['A' + str(3+i)].value)

    #日期
    bank_date = sheet['B' + str(3+i)].value
    # print(sheet['B3'].value)
    bank_date_y = str(bank_date).split(' ')[0].split('-')[0]
    bank_date_m = str(bank_date).split(' ')[0].split('-')[1]
    bank_date_d = str(bank_date).split(' ')[0].split('-')[2]
    # print(bank_date_y,bank_date_m,bank_date_d)


    #出票金额
    bank_money = sheet['C' + str(3+i)].value
    # print(sheet['C' + str(3+i)].value)

    #出票人全称
    bank_out_name = sheet['D' + str(3+i)].value
    # print(sheet['D' + str(3+i)].value)

    #出票人开户银行
    bank_out_bname = sheet['E' + str(3+i)].value
    # print(sheet['E' + str(3+i)].value)

    #出票人账号
    bank_out_num = sheet['F' + str(3+i)].value
    # print(sheet['F' + str(3+i)].value)

    #收款人全称
    bank_in_name = sheet['G' + str(3+i)].value
    # print(sheet['G' + str(3+i)].value)

    #收款人开户银行
    bank_in_bname = sheet['H' + str(3+i)].value
    # print(sheet['H' + str(3+i)].value)

    #收款人账号
    bank_in_num = sheet['I' + str(3+i)].value
    # print(sheet['I' + str(3+i)].value)

    #票据种类
    bank_category = sheet['J' + str(3+i)].value
    # print(sheet['J' + str(3+i)].value)

    #票据张数
    bank_bill_count = sheet['K' + str(3+i)].value
    # print(sheet['K' + str(3+i)].value)

    #票据号码
    bank_bill_num = sheet['L' + str(3+i)].value
    # print(sheet['L' + str(3+i)].value)


    ##openpyxl修改Excel

    in_wb = load_workbook('招商银行进账单.xlsx')

    in_sheet = in_wb.active

    #写入出票时间-年份
    in_sheet['E2'] = bank_date_y

    #写入出票时间-月份并合并
    in_sheet['F2'] = bank_date_m
    in_sheet.merge_cells('F2:G2')

    #写入出票时间-日期并合并
    in_sheet['H2'] = bank_date_d
    in_sheet.merge_cells('H2:I2')

    #写入出票人全称并合并
    in_sheet['B3'] = bank_out_name
    in_sheet.merge_cells('B3:F3')

    #写入出票人开户行账号并合并
    in_sheet['B4'] = bank_out_num
    in_sheet.merge_cells('B4:F4')

    #写入出票人开户银行并合并
    in_sheet['B5'] = bank_out_bname
    in_sheet.merge_cells('B5:F5')

    #写入收款人全称并合并
    in_sheet['K3'] = bank_in_name
    in_sheet.merge_cells('K3:W3')

    #写入收款人账号并合并
    in_sheet['K4'] = bank_in_num
    in_sheet.merge_cells('K4:W4')

    #写入收款人开户银行并合并
    in_sheet['K5'] = bank_in_bname
    in_sheet.merge_cells('K5:W5')

    #写入出票金额阿拉伯数字
    # bank_money
    #将金额分为两部分，大单位元和小单位角、分
    money_str = str(bank_money).split('.')
    # print('-------------第%s个的金额部分==%s==-----------'%(bank_serial,money_str))
    if len(money_str) == 2:
        #part1为元
        money_part1 = list(money_str[0])
        # print(money_part1)
        #part2为角和分
        money_part2 = list(money_str[1])
        # print(money_part2)
    else:
        #part1为元
        money_part1 = list(money_str[0])
        # print(money_part1)
        #part2为角和分
        money_part2 = ['0','0']
        # print(money_part2)

    #开始写入（从后往前写）
    in_sheet['V7'] = money_part2[1]
    in_sheet['U7'] = money_part2[0]

    #开始写入大单位数据
    in_sheet['T7'] = money_part1[-1]
    in_sheet['S7'] = money_part1[-2]
    if len(money_part1) >= 3:
        in_sheet['R7'] = money_part1[-3]
        in_sheet['Q7'] = '￥'

    if len(money_part1) >= 4:
        in_sheet['Q7'] = money_part1[-4]
        in_sheet['P7'] = '￥'

    if len(money_part1) >= 5:
        in_sheet['P7'] = money_part1[-5]
        in_sheet['O7'] = '￥'

    if len(money_part1) >= 6:
        in_sheet['O7'] = money_part1[-6]
        in_sheet['N7'] = '￥'

    if len(money_part1) >= 7:
        in_sheet['N7'] = money_part1[-7]
        in_sheet['M7'] = '￥'

    if len(money_part1) >= 8:
        in_sheet['M7'] = money_part1[-8]
        in_sheet['L7'] = '￥'
    # if len(money_part1) >= 9:
    #     in_sheet['O7'] = money_part1[-9]

    money_chn = num_to_chn(bank_money)
    if money_chn[-1] == '元':
        money_chn = money_chn + '整'
        in_sheet['B7'] = money_chn
        # in_sheet.merge_cells('E6:K6')
        in_sheet.merge_cells('B7:K7')
    else:
        in_sheet['B7'] = money_chn
        # in_sheet.merge_cells('E6:K6')
        in_sheet.merge_cells('B7:K7')

    #写入票据种类并合并
    # in_sheet['D8'] = bank_category
    # in_sheet.merge_cells('D8:E8')

    #写入票据张数并合并
    # in_sheet['H8'] = bank_bill_count
    # in_sheet.merge_cells('H8:I8')

    #写入票据号码并合并
    # in_sheet['D9'] = bank_bill_num
    # in_sheet.merge_cells('D9:I9')

    in_wb.save('result/%s.xlsx'%bank_serial)
    print("------------第%s个已经写入完成-------------"%bank_serial)