# !/usr/bin/python
# -*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/19

import time
import os
import re
import requests
from fake_useragent import UserAgent
from tqdm import tqdm
from progressbar import *

#获取文件内容
def get_file(dir_path):
    """
    Read M3u8 file and storge in memery
    :param dir_path:
    :return:
    """
    with open(dir_path,'r',encoding='gbk') as audio_file:
        audio_files = audio_file.readlines()

    return audio_files


def get_response(file_url):
    user_agent = UserAgent().random
    header = {
        "user-agent": user_agent,
    }
    # progressbar('This is M3u8 URL {}'.format(file_url))
    response = requests.get(url=file_url, headers=header)
    # character_block = file_url.split('/')[-1].split('-')[0]

    try:
        if response.status_code == 200:
            # print(response.text)
            content = response.text
            return content

    except Exception as e:
        print(e)


def write_ts_file(file_name, content, audio_dir_path):
    """
    Write parse html’s content
    :param file_name:
    :param content:
    :return:
    """

    audio_dir_path = audio_dir_path + '\\' + file_name
    if not os.path.exists(audio_dir_path):
        os.mkdir(audio_dir_path)

    with open(audio_dir_path + '\\' + '{}.txt'.format(file_name), 'w', encoding='utf-8') as f:
        f.write(content)


def read_ts_file(file_name, audio_dir_path):
    """
    Read ts file and join the ts url
    :param file_name:
    :return:
    """
    audio_dir_path = audio_dir_path + '\\' + file_name
    source_url = 'https://qrcode-file.oss-cn-shanghai.aliyuncs.com/streaming_file/'
    with open(audio_dir_path + '\\' + '{}.txt'.format(file_name), 'r', encoding='utf-8') as f:
        ts_content = f.readlines()
    ts_lst = []
    for line in ts_content:
        if '.ts' in line:
            ts_lst.append(source_url + line.strip('\n'))

    return ts_lst


def download_audio(file_name,ts_lst,audio_dir_path):
    # ts_dir_path = r'E:\study\python\Python-Exercise\project-spider-data\Branch\FileDir\AudioFile'
    ts_path = audio_dir_path + '\\' + file_name
    #
    # if not os.path.exists(ts_path):
    #     os.mkdir(ts_path)

    for ts_url in ts_lst:
        url_block = ts_url.split('-')[-1].split('.')
        ts_name = url_block[0]

        #download ts
        user_agent = UserAgent().random
        headers = {
            "user-agent": user_agent,
        }
        ts_content = requests.get(url=ts_url,headers=headers)
        total_size = int(ts_content.headers.get('content-length', 0))

        block_size = 1024   #kibibyte
        progress_bar = tqdm(total=total_size,unit='iB',unit_scale=True)

        with open(ts_path + '\\'+ '{}.ts'.format(ts_name),'wb') as ts:
            for chunk in ts_content.iter_content(chunk_size=block_size):
                if chunk:
                    progress_bar.update(len(chunk))
                    ts.write(chunk)

        progress_bar.close()
        time.sleep(0.3)

    print("\"{}".format(30*'-') + file_name + "\"" + "already downloaded")


def merge_ts(file_name,audio_dir_path):
    audio_dir_path = audio_dir_path + '\\' + file_name
    os.chdir(audio_dir_path)
    os.system("copy /b *.ts {}.mp3".format(file_name))


def main():

    # dir_path = r'E:\study\python\Python-Exercise\project-spider-data\Branch\FileDir\AudioFile\2020-11-19（88）'
    dir_path = r'E:\study\spider\Branch\FileDir\AudioFile\2021-03-25（83）'
    audio_dir_path = r'E:\study\spider\Branch\FileDir\AudioFile\TsFile'

    audio_files = get_file(dir_path)
    progress = ProgressBar()

    length = len(audio_files)
    # for lth in progress(range(length)):
    for file in audio_files:
        # file = audio_files[lth]
        file = file.strip('\n')
        file = file.split('#####')
        file_name = file[0]
        file_url = file[1]
        content = get_response(file_url)
        write_ts_file(file_name, content,audio_dir_path)
        ts_lst = read_ts_file(file_name,audio_dir_path)
        download_audio(file_name,ts_lst,audio_dir_path)
        merge_ts(file_name,audio_dir_path)
        time.sleep(3)

if __name__ == '__main__':
    main()