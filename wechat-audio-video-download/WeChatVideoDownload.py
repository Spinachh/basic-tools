# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/11/16

import time
import os
import re
import json
import requests
from urllib.request import urlretrieve
from fake_useragent import UserAgent
from tqdm import tqdm

def get_file(dir_path):
    # dir_path = r'E:\study\python\Python-Exercise\project-spider-data\Branch\FileDir\2020-11-16（68）'
    file_names = os.listdir(dir_path)

    return file_names

def get_url(file,dir_path):
    file = dir_path + '\\' + file
    with open(file,'r',encoding='utf-8') as f:
        html_content = f.readlines()
    # print(type(html_content))
    html_content = str(html_content)
    file_name = re.findall(r'class="play-title">(.*?)</p>',html_content,re.M|re.S)[0]

    file_url = re.findall(r'preload="preload" src=(.*?) style=',html_content)[0]
    file_url = file_url.replace('"','')

    return file_name,file_url

def Schedule(a, b, c):
    """
    :progress bar
    :param a:
    :param b:
    :param c:
    :return:
    """
    per = 100.0 * a * b / c
    if per > 100:
        per = 1
        print(" " + "%.2f%% Size of the downloaded: %ld File size: %ld" % (per, a * b, c) + '\r')


def get_video_http(file_name,file_url):
    download_path = r'E:\study\spider\Branch\VideoDir\part1'
    file_name = download_path + '\\' + file_name

    try:
        print("\"" + file_name + "\"" + "Starting download")
        urlretrieve(file_url, file_name, reporthook=Schedule)
        print("\"" + file_name + "\"" + "already downloaded")
    except Exception as e:
        print(e)


def get_video_https(file_name,file_url):

    download_path = r'E:\study\spider\Branch\VideoDir\part1'
    file_name = download_path + '\\' + file_name + '.mp4'
    headers = {'User-Agent': UserAgent().random,}

    try:
        print("\"" + file_name + "\"" + "Starting download")
        video_content = requests.get(url=file_url,headers=headers,stream=True,timeout=30)

        total_size = int(video_content.headers.get('content-length',0))
        block_size = 1024   #kibibyte
        progress_bar = tqdm(total=total_size,unit='iB',unit_scale=True)

        with open(file_name,'wb') as mp4:
            for chunk in video_content.iter_content(chunk_size=block_size):
                if chunk:
                    progress_bar.update(len(chunk))
                    mp4.write(chunk)

        progress_bar.close()

        print("\"" + file_name + "\"" + "already downloaded")
    except Exception as e:
        print(e)


def main():
    """
    http_version(video_download)
    :return:
    """

    dir_path = r'E:\study\spider\Branch\FileDir\2021-03-27（122）'

    files_names = get_file(dir_path)
    # print(files_names)
    for file in files_names:
        file_name,file_url = get_url(file,dir_path)
        get_video_https(file_name,file_url)
        # print(file_name,file_url)
        time.sleep(12)
        # break
        
if __name__ == '__main__':
    main()