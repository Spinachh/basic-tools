# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/12/02

import io
import sys
import os
import time
import logging

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
file_time = time.strftime("%Y-%m-%d", time.localtime())

img_dir_name = 'G:\\mongoole\\Data-Target\\'  # img directory name
# img_dir_name = 'G:\\mongoole\\SYS-Data-Target\\'  # img directory name
# img_dir_name = 'I:\\gfc\\修改名称后的照片\\'  # img directory name

# imglist = os.listdir(img_dir_name)
# DX-ZXF0820 (589)到DX-ZXF0820 (1329)

with open(r'sys-no\no-img-close-error-2022-11-28.txt', encoding='utf-8') as f:
    results = f.readlines()

for name in results:
    img_name = name.strip()
    path_file_name = img_dir_name + '\\' + img_name
    print(path_file_name)
    breakpoint()
    if os.path.exists(path_file_name):
        with open(r'sys-no\find-close-img-exist-result2.txt', 'a', encoding='utf-8') as f:
            f.write(path_file_name + '\n')
        print('此文件存在:', path_file_name)
    else:
        with open(r'sys-no\find-close-img-no-result2.txt', 'a', encoding='utf-8') as f:
            f.write(path_file_name + '\n')
        print('未找到此文件:', path_file_name)
